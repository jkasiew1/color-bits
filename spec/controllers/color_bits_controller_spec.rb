require "rails_helper"

RSpec.describe ColorBitsController, :type => :controller do
  describe "GET #index" do
    it "initializes the RGB color session variable with a random tuple" do
      get :index
      expect(session[:rgb_tuple_queue].size).to be(1)
    end
    it "adds another random tuple to an existing queue" do
      2.times { get :index }

      expect(session[:rgb_tuple_queue].size).to be(2)
    end
    it "adds a new random tuple at the end of the queue" do
      get :index
      last_tuple = session[:rgb_tuple_queue].last
      get :index

      expect(session[:rgb_tuple_queue].last).to_not eq(last_tuple)
    end
  end

  describe "GET #start" do
    it "only assigns a value if the queue is present" do
      xhr :get, :start

      expect(assigns(:rgb_tuple)).to be(nil)
    end

    it "assigns and returns the first tuple of the queue" do
      session[:rgb_tuple_queue] = ["0, 0, 0", "1, 1, 1", "2, 2, 2"]
      xhr :get, :start

      expect(assigns(:rgb_tuple)).to eq("0, 0, 0")
    end
    it "removes the first tuple from the queue" do
      session[:rgb_tuple_queue] = ["0, 0, 0", "1, 1, 1", "2, 2, 2"]
      xhr :get, :start

      expect(session[:rgb_tuple_queue].first).to eq("1, 1, 1")
    end
  end
end