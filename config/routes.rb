Rails.application.routes.draw do
  root 'color_bits#index'
  get '/start', to: 'color_bits#start', :as => 'start'
end
