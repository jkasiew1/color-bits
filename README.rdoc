== colorBits

colorBits is my solution to the littleBits code challenge. Some organizational decisions I made due to the scope of the project include:

* inline javascript for quick reference in the view

* leaving logic in the controller since queue data is stored in a session variable

== Getting Started

To install and run:

1. cd color-bits
2. bundle install
3. rspec
4. rails s

== Heroku site

http://color-bits.herokuapp.com/


