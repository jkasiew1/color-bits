class ColorBitsController < ApplicationController
  def index
    session[:rgb_tuple_queue] ||= []
    session[:rgb_tuple_queue] << random_rgb_tuple
  end

  def start
    @rgb_tuple = session[:rgb_tuple_queue].shift if session[:rgb_tuple_queue].present?
  end

  private

  def random_rgb_tuple
    "#{random_color_value}, #{random_color_value}, #{random_color_value}"
  end

  def random_color_value
    rand(0..255)
  end
end